# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import numpy as np
import math3d as m3d

from pymoco.kinematics import joints
from pymoco.robot.robotdefinition import RobotDefinition


class Emika(RobotDefinition):

    def __init__(self, **kwargs):
        RobotDefinition.__init__(self, **kwargs)

        self._dof = 7

        # Joint position limits
        self._pos_lim_act = np.deg2rad(np.array(
            [[-170, 170],
             [-105, 105],
             [-170, 170],
             [-180, 5],
             [-170, 170],
             [-5, 219],
             [-170, 170]],
            dtype=np.double)).T

        # Joint speed limits
        self._spd_lim_act = np.deg2rad(np.array(
            [150, 150, 150, 150, 180, 180, 180],
            dtype=np.double)).T

        # Home pose
        self._q_home = np.zeros(self._dof, dtype=np.float64)
        # self._q_home[4] = -np.pi/2
        # self._q_home[6] = np.pi/2

        # Internal link transform for the eight robot parts.
        self._link_xforms = [m3d.Transform() for i in range(self._dof + 1)]

        self._link_xforms[0].pos.z = 0.333  # 0.1419

        self._link_xforms[1].pos.z = 0  # - self._link_xforms[0].pos.z
        self._link_xforms[1].orient.rotate_xb(-np.pi/2)

        self._link_xforms[2].pos.y = -0.316
        self._link_xforms[2].orient.rotate_xb(np.pi/2)

        self._link_xforms[3].pos.x = 0.0825
        self._link_xforms[3].orient.rotate_xb(np.pi/2)

        self._link_xforms[4].pos.x = -0.0825
        self._link_xforms[4].pos.y = 0.384
        self._link_xforms[4].orient.rotate_xb(-np.pi/2)

        self._link_xforms[5].orient.rotate_xb(np.pi/2)

        self._link_xforms[6].pos.x = 0.088
        self._link_xforms[6].orient.rotate_xb(np.pi/2)

        self._link_xforms[7].pos.z = 0.107

        self._joint_xforms = self.joint_xforms
        
    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(7)]
    joint_xforms = property(get_joint_xforms)
